import Model from './model';
import View from './view';
import Controller from './controller';

const model = new Model();
const view = new View();
const controller = new Controller(model, view);

const url = new URL(window.location.href);
const queryId = url.searchParams.get('id');

if (queryId) {
  controller.picture(queryId);  
} else {
  controller.init();
}
