class Controller {
  constructor(model, view) {
    this.model = model;
    this.view = view;

    model.on('wsPic', this._wsOnPic.bind(this));
    model.on('wsComment', this._wsOnComment.bind(this));
    model.on('wsMask', this._wsOnMask.bind(this));

    view.on('pictureAdd', this._pictureOnAdd.bind(this));

    view.on('menuChangeMode', this._menuOnChangeMode.bind(this))
    view.on('menuChangeCommentsVisability', this._menuOnChangeCommentsVisability.bind(this));
    view.on('menuChangeColor', this._menuOnChangeColor.bind(this))

    view.on('commentsFormAdd', this._commentsFormOnAdd.bind(this));
    view.on('commentsSubmit', this._commentsOnSubmit.bind(this));

    view.on('canvasClosePath', this._canvasOnClosePath.bind(this));
  }

   /* ~~~~~~~~~~~~~~~~~~~~ Canvas Handlers ~~~~~~~~~~~~~~~~~~~~~~ */

   _canvasOnClosePath(canvas) {
     this.model.sendWebSocketCanvas(canvas);
   }

   /* ~~~~~~~~~~~~~~~~~~~~ WebSocket Handlers ~~~~~~~~~~~~~~~~~~~~~~ */

   _wsOnPic(model) {
    this.model = model;
    this.view.pictureRender({
      picture: this.model.getPicture(),
    })
    .then(() => {
      this.view.commentsFormsRender({
        visability: this.model.getCommentsVisability(),
        list: this.model.getCommentsList()
      })
    })
    .then(() => {
      this.view.maskRender({
        mask: this.model.getMask()
      })
    })
  }

  _wsOnComment(comments) {
    this.view.commentsFormsRender({
      visability: this.model.getCommentsVisability(),
      list: this.model.getCommentsList()
    })
  }

  _wsOnMask(mask) {
    this.view.maskRender({
      mask: mask
    })
    .then(() => {
      this.view.canvasClear();  
    })
  }

   /* ~~~~~~~~~~~~~~~~~~~~ Picture Handlers ~~~~~~~~~~~~~~~~~~~~~~ */

   _pictureOnAdd(file) {
    if (file.type !== 'image/jpeg' && file.type !== 'image/png') {
      this.view.errorShow();
      return;
    } 
    
    this.view.errorHide();

    this.view.pictureShowLoader();
    this.model.addPicture(file)
      .then(picture => {
        this.model.createWebSocket(picture.id);
        this.model.deleteMask();
        this.view.maskRemove();
        this.view.canvasRemove();
        this.model.scanMenuColor();
        this.view.menuRender({
          state: this.model.updateAppState('review'),
          mode: this.model.updateMenuMode('share'),
          idForLink: picture.id
        });
        this.view.pictureHideLoader();
      })
      .catch(err => console.log(err));
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Menu Handlers ~~~~~~~~~~~~~~~~~~~~~~ */

  _menuOnChangeMode(mode) {
    this.view.menuRender({
      state: this.model.getAppState(),
      mode: this.model.updateMenuMode(mode),
      idForLink: this.model.getPicture().id
    });

    if (mode === 'draw') {
      this.view.canvasAdd({
        color: this.model.getMenuColor()
      });
    } else {
      this.view.canvasRemove();
    }
  }

  _menuOnChangeCommentsVisability(visability) {
    
    this.view.commentsTogleVisability({
      visability: this.model.updateCommentsVisability(visability),
    });
  }

  _menuOnChangeColor(color) {
    this.model.updateMenuColor(color);
    this.view.canvasRemove();
    this.view.canvasAdd({
      color: this.model.getMenuColor()
    });
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Comments Handlers ~~~~~~~~~~~~~~~~~~~~~~ */

  _commentsFormOnAdd({ left, top }) {
    this.view.commentsFormAdd({
      mode: this.model.getMenuMode(),
      visability: this.model.getCommentsVisability(),
      left,
      top
    })
  }

  _commentsOnSubmit({ commentsForm, message }) {
    this.view.commentsShowLoader(commentsForm);
    const {id: pictureId} = this.model.getPicture();
    const left = parseFloat(commentsForm.dataset.left);
    const top = parseFloat(commentsForm.dataset.top);

    this.model.sendComment({pictureId, message, left, top})
      .then((comments) => {
        this.view.commentsHideLoader(commentsForm);
      });
  }
  
  /* ~~~~~~~~~~~~~~~~~~~~ Init ~~~~~~~~~~~~~~~~~~~~~~ */

  init() {
    this.view.init();
    const pictureCurrent = this.model.getPicture();

    if (!pictureCurrent) {
      this.view.menuRender({
        state: this.model.updateAppState('initial'),
      });
    }

    if (pictureCurrent) {
      this.model.scanMenuColor();
      this.view.menuRender({
        state: this.model.updateAppState('review'),
        mode: this.model.updateMenuMode('share'),
        idForLink: pictureCurrent.id
      });

      this.model.createWebSocket(pictureCurrent.id);
    }
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Picture ~~~~~~~~~~~~~~~~~~~~~~ */

  picture(queryPictureId) {
    this.view.init();
    this.model.requestPictureInfo(queryPictureId)
    .then((data) => {
      this.model.scanMenuColor();
      const pictureCurrent = data.picture;
      this.view.menuRender({
        state: this.model.updateAppState('review'),
        mode: this.model.updateMenuMode('comments'),
        idForLink: pictureCurrent.id
      });
      this.model.createWebSocket(pictureCurrent.id);
    })
  }
}

export default Controller;
