import { c, Utility, EventEmitter } from './helpers';

class Model extends EventEmitter {
  constructor(state = 'publish') {
    super();

    this.app = {
      state,
    };

    this.picture = null;

    this.menu = {
      mode: null,
      color: null,
      link: null
    };

    this.mask = {
      url: null
    }

    this.comments = {
      visability: null,
      list: null
    };

    this.webSocket = null;
  }

  /* ~~~~~~~~~~~~~~~~~~~~ App Model Methods ~~~~~~~~~~~~~~~~~~~~~~ */

  getAppState() {
    return this.app.state;
  }

  updateAppState(state) {
    this.app.state = state;
    return this.app.state;
  }

   /* ~~~~~~~~~~~~~~~~~~~~ Mask Model Methods ~~~~~~~~~~~~~~~~~~~~~~ */

   getMask() {
     return this.mask.url;
   }

   deleteMask() {
    this.mask.url = null;
   }
  

  /* ~~~~~~~~~~~~~~~~~~~~ Picture Model Methods ~~~~~~~~~~~~~~~~~~~~~~ */

  addPicture(file) {
    const formData = new FormData();
    formData.append('title', file.name);
    formData.append('image', file);

    return fetch('https://neto-api.herokuapp.com/pic', {
      method: "POST",
      body: formData,
    })
      .then(res => {
        if (res.status >= 200 && res.status < 300) {
          return res;
        }
        throw new Error (res.statusText);
      })
      .then(res => res.json())
      .then(picture => {
        this.updatePicture(picture);
        return this.picture;
      })
  }

  updatePicture({id, title, url, timestamp}) {
    this.picture = {
      id,
      title,
      url,
      timestamp
    };
    localStorage.setItem('picture', JSON.stringify(this.picture));
    return this.picture;
  }

  getPicture() {
    return this.picture || (JSON.parse(localStorage.getItem('picture')));
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Menu Model Methods ~~~~~~~~~~~~~~~~~~~~~~ */

  scanMenuColor() {
    const checkedColorElem = Array.from(document.querySelectorAll('.menu__color'))
      .find(el => el.checked);
    this.menu.color = checkedColorElem ? Utility.stringToHex(checkedColorElem.value) : null;
    return this.menu.color;
  }

  getMenuColor() {
    return this.menu.color;
  }

  updateMenuColor(color) {
    this.menu.color = Utility.stringToHex(color);
    return this.menu.color;
  }

  updateMenuMode(mode) {
    this.menu.mode = mode;
    return this.menu.mode;
  }

  getMenuMode() {
    return this.menu.mode;
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Comments Model Methods~~~~~~~~~~~~~~~~~~~~~~ */

  sendComment({pictureId, message, left, top}) {
    const comment = {
      message,
      left,
      top
    }

    var formBody = [];
    for (var property in comment) {
      var encodedKey = encodeURIComponent(property);
      var encodedValue = encodeURIComponent(comment[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    return fetch(`https://neto-api.herokuapp.com/pic/${pictureId}/comments`, {
      method: 'POST', 
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: formBody
    })
      .then(res => res.json())
      .then(this.parsePictureInfo.bind(this))
      .then(() => {
        return this.comments;
      })
      .catch(err => console.log(err));
  }


  updateCommentsVisability(visability) {
    this.comments.visability = visability;
    return this.comments.visability;
  }

  getCommentsVisability() {
    if (document.querySelector('.menu__toggle').checked) {
      this.comments.visability = true;
    } else {
      this.comments.visability = false;
    }
    return this.comments.visability;
  }

  getCommentsList() {
    return this.comments.list;
  }


  /* ~~~~~~~~~~~~~~~~~~~~ WebSocket Methods~~~~~~~~~~~~~~~~~~~~~~ */

  createWebSocket(pictureId) {
    if (this.webSocket) {
      delete this.webSocket;
    }
    this.webSocket = new WebSocket(`wss://neto-api.herokuapp.com/pic/${pictureId}`);

    this.webSocket.addEventListener('message', ev => {
      const data = JSON.parse(ev.data);
      console.log(data);
      if (data.event === 'pic') {
        this.parsePictureInfo(data.pic)
        this.emit('wsPic', this);
      }
      if (data.event === 'comment') {
        const { id, ...newComment } = data.comment;
        this.comments.list[id] = newComment;
        this.emit('wsComment', this.comments);
      }
      if (data.event === 'mask') {
        this.mask.url = data.url;
        this.emit('wsMask', this.mask.url);
      }
    })

    this.webSocket.addEventListener('error', error => {
      console.log(error);
    })
  }

  sendWebSocketCanvas(canvas) {
    canvas.toBlob(blob => this.webSocket.send(blob));
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Common Model Methods~~~~~~~~~~~~~~~~~~~~~~ */

  requestPictureInfo(id) {
    return this.fetchPictureInfo(id)
      .then(this.parsePictureInfo.bind(this));
  }

  fetchPictureInfo(id) {
    return fetch(`https://neto-api.herokuapp.com/pic/${id}`)
      .then(res => res.json())
      .then(data => data)
  }

  parsePictureInfo(mainData) {
    const { id, title, url, mask, timestamp, comments } = mainData;
    this.picture = {
      id,
      title,
      url,
      timestamp
    };

    this.mask.url = mask;

    this.comments.list = comments || {};
    return this;
  }
}

export default Model;