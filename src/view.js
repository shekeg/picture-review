import { c, Utility, EventEmitter } from './helpers';

class View extends EventEmitter {
  constructor() {
    super();
    this.app = document.querySelector('.app');

    this.menu = document.querySelector('.menu');
    this.menuDrag = this.menu.querySelector('.drag');
    this.menuBurger = this.menu.querySelector('.burger');
    this.menuNew = this.menu.querySelector('.new');
    this.menuNewInput = document.createElement('input');
    this.menuNewInput.type = 'file';
    this.menuComments = this.menu.querySelector('.comments');
    this.menuCommentsVisability = this.menu.querySelectorAll('.menu__toggle');
    this.menuDraw = this.menu.querySelector('.draw');
    this.menuColors = this.menu.querySelectorAll('.menu__color');
    this.menuShare = this.menu.querySelector('.share');

    this.pictureCurrent = this.app.querySelector('.current-image');
    this.pictureLoader = this.app.querySelector('.image-loader');

    this.error = this.app.querySelector('.error');

    this.commentsForm = document.querySelector('.comments__form');

    this._addEventListeners();
  }

  _addEventListeners() {
    this.menuDrag.addEventListener('mousedown', this._menuDragOnMouseDown.bind(this));
    this.menuDrag.addEventListener('mousemove', this._menuDragOnMouseMove.bind(this));
    this.menuDrag.addEventListener('mouseup', this._menuDragMouseUp.bind(this));

    this.menuBurger.addEventListener('click', this._menuBurgerOnClick.bind(this));

    this.menuNew.addEventListener('click', this._menuNewOnClick.bind(this));
    this.menuNewInput.addEventListener('change', this._menuNewInputOnChange.bind(this));
    
    this.menuComments.addEventListener('click', this._menuCommentsOnClick.bind(this));
    this.menuDraw.addEventListener('click', this._menuDrawOnClick.bind(this));
    this.menuShare.addEventListener('click', this._menuShareOnClick.bind(this));

    this.menuCommentsVisability.forEach(visabilityToggle => {
      visabilityToggle.addEventListener('click', this._menuCommentsVisabilityOnClick.bind(this));
    })

    this.menuColors.forEach(colorInput => {
      colorInput.addEventListener('click', this._menuColorOnClick.bind(this), true);
    })

    this.app.addEventListener('click', (ev) => {

      if (ev.target.classList.contains('current-image') || ev.target.classList.contains('mask')) {
        this._commentsFormAddOnClick(ev);
      }
      
      if (ev.target.classList.contains('comments__marker-checkbox')) {
        this._commentsMarkerOnChange(ev);
      }

      if (ev.target.classList.contains('comments__close')) {
        this._commentsCloseOnClick(ev);
      }

      if (ev.target.classList.contains('comments__submit')) {
        this._commentsSubmitOnClick(ev);
      }

      if (ev.target.classList.contains('menu_copy')) {
        this._menuCopyOnClick(ev);
      }

    })
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Init ~~~~~~~~~~~~~~~~~~~~~~ */

  init() {
    this.pictureCurrent.dataset.id = '';
    this.pictureCurrent.src = '';

    this.commentsForm.querySelectorAll('.comment').forEach(comment => comment.remove());
    this.commentsForm.remove();
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Error View ~~~~~~~~~~~~~~~~~~~~~~ */

  errorShow() {
    this.error.style.display = 'block';
  }
  
  errorHide() {
    this.error.style.display = 'none';
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Picture View ~~~~~~~~~~~~~~~~~~~~~~ */

  pictureRender({ picture }) {
    return new Promise((done, fall) => {
      const { id, url } = picture;
      this.pictureCurrent.dataset.id = id;
      this.pictureCurrent.src = url;
      this.pictureCurrent.addEventListener('load', () => {
        done();
      })
    })
  }

  pictureShowLoader() {
    this.pictureLoader.style.display = 'block';
  }

  pictureHideLoader() {
    this.pictureLoader.style.display = 'none';
  }

  _getPictureOffset() {
    const pictureCurrentRect = this.pictureCurrent.getBoundingClientRect();
    return {
      offsetLeft: pictureCurrentRect.left,
      offsetTop: pictureCurrentRect.top
    }
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Mask View ~~~~~~~~~~~~~~~~~~~~~~ */

  maskRender({ mask }) {
    if (mask) {
      return new Promise((done) => {
        if (!this.mask) {
          this.mask = document.createElement('img');
        }
        
        this.mask.src = mask;
        this.mask.classList.add('mask');
    
        const picRect = this.pictureCurrent.getBoundingClientRect();
    
        this.mask.width = picRect.width;
        this.mask.height = picRect.height;
    
        this.mask.style.position = 'absolute';
        this.mask.style.top = '50%';
        this.mask.style.left = '50%';
        this.mask.style.transform = 'translate(-50%, -50%)';
        this.mask.style.zIndex = '1';
    
        this.app.insertBefore(this.mask, this.pictureCurrent);

        this.mask.addEventListener('load', () => {
          done();
        })
      })
    }
  }

  maskRemove() {
    if (this.mask) {
      this.mask.remove();
      this.mask.url = '';
    }
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Canvas View ~~~~~~~~~~~~~~~~~~~~~~ */

  canvasAdd({ color }) {
    this.canvas = document.createElement('canvas');
    this.ctx = this.canvas.getContext('2d');
    this.canvas.classList.add('canvas');

    const picRect = this.pictureCurrent.getBoundingClientRect();

    this.canvas.width = picRect.width;
    this.canvas.height = picRect.height;

    this.canvas.style.position = 'absolute';
    this.canvas.style.top = '50%';
    this.canvas.style.left = '50%';
    this.canvas.style.transform = 'translate(-50%, -50%)';
    this.canvas.style.zIndex = '2';

    this.app.insertBefore(this.canvas, this.pictureCurrent);

    let drawing = false;
    
    let lastX =  null;
    let lastY =  null;
    
    this.canvas.addEventListener('mousedown', () => {
      drawing = true;
    })

    this.canvas.addEventListener('mousemove', (ev) => {
      if (drawing) {
        this.ctx.beginPath();
        this.ctx.moveTo(ev.offsetX, ev.offsetY);
        this.ctx.strokeStyle = color;
        this.ctx.lineWidth = 4;
        this.ctx.lineJoin = "round";
        this.ctx.lineCap = "round";
  
        if (lastX && lastY) {
          this.ctx.moveTo(lastX, lastY);
          this.ctx.lineTo(ev.offsetX, ev.offsetY);
          this.ctx.stroke();
        }

        this.ctx.closePath();
        [lastX, lastY] = [ev.offsetX, ev.offsetY];
      }
    })

    this.canvas.addEventListener('mouseup', () => {
      drawing = false;
      lastX =  null;
      lastY =  null;
      this.emit('canvasClosePath', this.canvas);
    })

    this.canvas.addEventListener("mouseleave", (ev) => {
      if (drawing) {
        const canvasRect = this.canvas.getBoundingClientRect();
        if (ev.clientX < canvasRect.left || ev.clientX > canvasRect.right ||
            ev.clientY < canvasRect.top || ev.clientY > canvasRect.bottom) {
            drawing = false;
            lastX =  null;
            lastY =  null;
            this.emit('canvasClosePath', this.canvas);
        }
      }
    });
  }

  canvasClear() {
    if (this.ctx) {
      this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    }
  }

  canvasRemove() {
    document.querySelectorAll('.canvas').forEach(canvas => {
      canvas.remove();
    })
    // if (this.canvas) {
    //   this.canvas.remove();
    // }
  }


  /* ~~~~~~~~~~~~~~~~~~~~ Comments View ~~~~~~~~~~~~~~~~~~~~~~ */

  commentsFormsRender({ visability, list }) {
    if (list) {
      this.app.querySelectorAll('.comments__form').forEach(form => form.remove());
      const commentsArr = [];

      Object.keys(list).forEach(key => {
        commentsArr.push({ id: key, ...list[key] });
      })

      commentsArr.forEach(comment => {
        let commentsForm = document.querySelector(`.comments__form[data-left="${comment.left}"][data-top="${comment.top}"]`);
        if (!commentsForm) {
          commentsForm = this.commentsForm.cloneNode(true);
          commentsForm.dataset.left = comment.left;
          commentsForm.dataset.top = comment.top;
          
          const {offsetLeft, offsetTop} = this._getPictureOffset();
          commentsForm.style.left = comment.left + offsetLeft - 22 + 'px';
          commentsForm.style.top = comment.top + offsetTop - 10 + 'px';
        }

        this.commentRender({ commentsForm, comment });

        visability ? commentsForm.style.display = 'block' : commentsForm.style.display = 'none';

        this.app.appendChild(commentsForm);
      })
    }
  }

  commentRender({ commentsForm, comment }) {

    const date = new Date(comment.timestamp);
    const dateFormatted = `${date.toLocaleDateString('RU-ru')} ${date.toLocaleTimeString('RU-ru')}`;
    const message = comment.message;

    const commentElem = Utility.browserJSEngine(this._commentJSTemplate({
      dateFormatted,
      message
    }));

    const commentsBody = commentsForm.querySelector('.comments__body');
    commentsBody.insertBefore(commentElem, commentsBody.firstChild);
  }

  commentsFormAdd({mode, visability, left, top}) {
    if (mode !== 'comments' || !visability) {
      return;
    }

    const commentsFormClone = this.commentsForm.cloneNode(true);
    commentsFormClone.dataset.left = left;
    commentsFormClone.dataset.top = top;
    
    const {offsetLeft, offsetTop} = this._getPictureOffset();

    const bodyLeft = left + offsetLeft - 22;
    const bodyTop = top + offsetTop - 10;

    commentsFormClone.style.left = bodyLeft + 'px';
    commentsFormClone.style.top = bodyTop + 'px';

    this.app.appendChild(commentsFormClone);
  }

  commentsTogleVisability({ visability }) {
    this.app.querySelectorAll('.comments__form').forEach(commentsForm => {
      if (!visability) {
        commentsForm.style.display = 'none';
      }
      if (visability) {
        commentsForm.style.display = 'block';
      }
    })
  }

  commentsShowLoader(commentsForm) {
    const commentsBody = commentsForm.querySelector('.comments__body');
    const commentsLoader = Utility.browserJSEngine(this._commentLoaderJSTemplate());
    commentsBody.insertBefore(commentsLoader, commentsBody.querySelector('.comments__input'));
  }

  commentsHideLoader(commentsForm) {
    commentsForm.querySelector('.comments__input').value = '';
    commentsForm.querySelector('.loader')
      .parentElement
      .remove();
  }

  _commentJSTemplate({ dateFormatted, message }) {
    return {
      tag: 'div',
      cls: 'comment',
      content: [
        {
          tag: 'p',
          cls: 'comment__time',
          content: dateFormatted
        },
        {
          tag: 'p',
          cls: 'comment__message',
          content: message
        },
      ]
    }
  }

  _commentLoaderJSTemplate() {
    return {
      tag: 'div',
      cls: 'comment',
      content: {
        tag: 'div',
        cls: 'loader',
        content: [
          {tag: 'span'},
          {tag: 'span'},
          {tag: 'span'},
          {tag: 'span'},
          {tag: 'span'},
        ]
      }
    }
  }

  _commentsFormAddOnClick(ev) {
    const pictureCurrentRect = this.pictureCurrent.getBoundingClientRect();
    const left = ev.clientX - pictureCurrentRect.left;
    const top = ev.clientY - pictureCurrentRect.top;
    
    this.emit('commentsFormAdd', {left, top});
  }

  _menuCommentsVisabilityOnClick({ target }) {
    if (target.value === 'on') {
      this.emit('menuChangeCommentsVisability', true);
    } else if (target.value === 'off') {
      this.emit('menuChangeCommentsVisability', false);
    }
  }

  _commentsMarkerOnChange({ target }) {
    this.app.querySelectorAll('.comments__marker-checkbox').forEach(commentsMarkerCheckbox => {
      if (target !== commentsMarkerCheckbox) {
        commentsMarkerCheckbox.checked = false;
      }
    })
  }

  _commentsCloseOnClick({ target }) {
    Utility.getSiblings(target.parentElement)
      .find(sibiling => sibiling.classList.contains('comments__marker-checkbox'))
      .checked = false;
  }

  _commentsSubmitOnClick(ev) {
    ev.preventDefault();
    const commentsForm = ev.target.parentElement.parentElement;
    const message = Utility.getSiblings(ev.target)
      .find(sibiling => sibiling.classList.contains('comments__input'))
      .value;
      
    this.emit('commentsSubmit', {commentsForm, message});
  }

  /* ~~~~~~~~~~~~~~~~~~~~ Menu View ~~~~~~~~~~~~~~~~~~~~~~ */

  menuRender({ state, mode, idForLink }) {
    if (state === 'review') {
      this.menu.dataset.state = 'selected';
      switch (mode) {
        case 'comments':
          this.menuComments.dataset.state = 'selected';
          break;
        case 'draw':
          this.menuDraw.dataset.state = 'selected';
          break;
        case 'share':
          this.menuShare.dataset.state = 'selected';
          break;
        default:
          break;
      }

      const url = new URL(window.location.href);
      this.menu.querySelector('.menu__url').setAttribute('value', `${url.origin}/?id=${idForLink}`);
    }

    if (state === 'initial') {
      this.menu.dataset.state = 'initial';
    }
  }

  _menuNewOnClick() {
    this.menuNewInput.click();
  }

  _menuNewInputOnChange(ev) {
    const file = ev.currentTarget.files[0];
    if (file) {
      this.emit('pictureAdd', file);
    }
  }

  _menuCopyOnClick(ev) {
    navigator.clipboard.writeText(this.menu.querySelector('.menu__url').value);
  }

  _menuBurgerOnClick(ev) {
    this.menuComments.dataset.state = '';
    this.menuDraw.dataset.state = '';
    this.menuShare.dataset.state = '';
    this.menu.dataset.state = 'default';
  }

  _menuCommentsOnClick() {
    this.emit('menuChangeMode', 'comments')
  }

  _menuDrawOnClick() {
    this.emit('menuChangeMode', 'draw')
  }

  _menuColorOnClick(ev) {
    ev.stopPropagation();
    this.emit('menuChangeColor', ev.target.value);
  }

  _menuShareOnClick() {
    this.emit('menuChangeMode', 'share')
  }

  _menuDragOnMouseDown(ev) {
    if (ev.target.classList.contains('drag')) {
      this.movedMenu = ev.target.parentElement;
    }
  }

  _menuDragOnMouseMove(ev) {
    Utility.throttleAnimationFrame((ev) => {
      if (this.movedMenu) {
        const { width: menuWidth, height: menuHeight } = this.movedMenu.getBoundingClientRect();
        const bodyWidth = document.body.offsetWidth;
        const bodyHeight = document.body.offsetHeight;

        let x = ev.pageX - this.menuDrag.offsetWidth / 2;
        let y = ev.pageY - this.movedMenu.offsetHeight / 2;

        y = (y > 0) ? y : 0;
        y = (y + menuHeight + 2 <= bodyHeight) ? y : bodyHeight - menuHeight;

        x = (x > 0) ? x : 0;
        x = (x + menuWidth + 2 <= bodyWidth) ? x : bodyWidth - menuWidth;

        this.movedMenu.style.setProperty('--menu-left', x + 'px');
        this.movedMenu.style.setProperty('--menu-top', y + 'px');
      }
    })(ev);
  }

  _menuDragMouseUp() {
    this.movedMenu = null;
  }

}

export default View;