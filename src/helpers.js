function c(target) {
  console.log(target);
}

class Utility {
  static throttleAnimationFrame(callback) {
    let isWaiting = false;
    return function() {
      if (!isWaiting) {
        callback.apply(this, arguments);
        isWaiting = true;
        requestAnimationFrame(() => {
          isWaiting = false;
        });
      }
    };
  }

  static throttle(callback, delay) {
    let isWaiting = false;
    return function() {
      if (!isWaiting) {
        callback.apply(this, arguments);
        isWaiting = true;
        setTimeout(() => {
          isWaiting = false;
        }, delay);
      }
    };
  }

  static browserJSEngine(block) {
    if (block === undefined || block === null || block === false) {
      return document.createTextNode("");
    }

    if (
      typeof block === "string" ||
      typeof block === "number" ||
      block === true
    ) {
      return document.createTextNode(block);
    }

    if (Array.isArray(block)) {
      return block.reduce((f, element) => {
        f.appendChild(this.browserJSEngine(element));
        return f;
      }, document.createDocumentFragment());
    }

    const element = document.createElement(block.tag || "div");
    element.classList.add(...[].concat(block.cls));
    if (block.attrs) {
      Object.keys(block.attrs).forEach(key => {
        element.setAttribute(key, block.attrs[key]);
      });
    }

    if (block.content) {
      if (block.cls === "comment-text") {
        const preWrap = document.createElement("pre");
        preWrap.appendChild(this.browserJSEngine(block.content));
        element.appendChild(preWrap);
      } else {
        element.appendChild(this.browserJSEngine(block.content));
      }
    }

    return element;
  }

  static getSiblings(elem) {
    var siblings = [];
    var sibling = elem.parentNode.firstChild;

    while (sibling) {
      if (sibling.nodeType === 1 && sibling !== elem) {
        siblings.push(sibling);
      }
      sibling = sibling.nextSibling;
    }
    return siblings;
  }

  static stringToHex(string) {
    switch (string) {
      case "red":
        return "#ea5d56";
      case "yellow":
        return "#f3d135";
      case "green":
        return "#6cbe47";
      case "blue":
        return "#53a7f5";
      case "purple":
        return "#b36ade";
    }
  }
}

class EventEmitter {
  constructor() {
      this.events = {};
  }

  on(type, listener) {
      this.events[type] = this.events[type] || [];
      this.events[type].push(listener);
  }

  emit(type, arg) {
      if (this.events[type]) {
          this.events[type].forEach(listener => listener(arg));
      }
  }
}



export {c, Utility, EventEmitter}